package Week2.repository;

import Week2.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository
        extends CrudRepository<Student, Long> {
    @Query(value = "select * from student where group = 10", nativeQuery = true)
    public List<Student> getStudentGroup10();
    List<Student> findAllByGroup(int group);

}


