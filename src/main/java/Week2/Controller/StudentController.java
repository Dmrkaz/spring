package Week2.Controller;

import Week2.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;
    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @GetMapping("/api/students")
    public ResponseEntity<?> getStudents(){
        return ResponseEntity.ok(studentRepository.findAll());
    }
    @GetMapping("/api/group10")
    public ResponseEntity<?> getStudentsGroup10(){
        return ResponseEntity.ok(studentRepository.getStudentGroup10());
    }
    @GetMapping("/api/students/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable int group){
        return ResponseEntity.ok(studentRepository.findAllByGroup(group));
    }

}